Root Directory is c:/etc/vimrc and should not be touched. it is the config file of vim installation. 
Home Directory is c:/User/lmoschos/.vimrc 
The vimrc file in the home directory is the .vimrc file which will naturally not exist and should be created 
as "user settings" config/vimrc file, which will overwrite the default settings in the root directory. 
Installation Instruction (all in home directory):
1.) make a directory called .vim (mkdir .vim)
enter the folder and make folders like:  autoload, backup, colors, dirs, plugged, pack
2.) make the .vimrc file (touch .vimrc or vim .vimrc)
3.) copy paste the config file from gitlab/github/bitbucket. 
3.) Install the packages which are in the .vimrc (config) file with command ":PlugInstall" in the .vimrc file
(additionally do ":source ~/.vimrc") 
4.) If in any other case you want to delete the .vimrc file (with rm -rf .vimrc in the home directory) - first delete the packages with 
the command ":PlugClean".
=> If the either PlugInstall or PlugClean is not working than try again and use sourcing (:source ~/.vimrc).
5.) Install for autocomplete the following:
a.) yarn (curl --compressed -o- -L https://yarnpkg.com/install.sh | bash)   
the Plug (Plug 'neoclide/coc.nvim', {'branch': 'release'}) should be already in the config file 
6.) By receiving error "The error is due to the fact that COC is missing some dependencies to function." The following should be done: 
a.) Navigate to the directory \nvim\plugged\coc.nvim
b.) Use your package manager: npm install or yarn install
c.) do yarn buildd.) :call coc#util#install()
with the command "~/.vimrc" the vimrc - which is the configuration file of vim - should be accessible from anywhere.  
remember: 
open left file tree with "\ and n" 
open right tags tab with "\ and t"
Useful links:
Vimrc Configuration Guide - How to Customize Your Vim Code Editor with Mappings, Vimscript, Status Line, and More
https://www.freecodecamp.org/news/vimrc-configuration-guide-customize-your-vim-editor/
Setting Up Vim to Work with Python Applications
https://www.youtube.com/watch?v=s0Bimr1079A&ab_channel=MiguelGrinberg 
https://gist.github.com/miguelgrinberg/527bb5a400791f89b3c4da4bd61222e4 
Vim setup for Python programmers: conquer of completion (coc) and pyright
https://www.youtube.com/watch?v=i_LhEHkDKs4&ab_channel=DongZhou 
https://github.com/neoclide/coc.nvim 
